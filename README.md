# Harmagedon (alpha)

A automated competition simulator.

## Adding a new competition

Start by forking the repository.

Then add your competition's code as a file in games e.g. `games/my_competition.py`

Add a readme which describes the competition's rules e.g. `games/my_competition.md`

See `games/tank.py` and `games/tank.md` as an example.

Create a merge request.

## Contributing

If you would like to contribute, please open an issue describing the change you would like to make.
