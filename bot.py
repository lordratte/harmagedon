import json

import sys
import subprocess

from pytio import Tio, TioRequest

from tools import LANGS, CONFIG


class BotConfigError(Exception):
    pass


class BotRunError(Exception):
    pass


def offline_python(code, inp):
    with subprocess.Popen([sys.executable, '-c', code],
                          stdin=subprocess.PIPE,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          text=True) as process:
        stdout, stderr = process.communicate(input=inp, timeout=3)

        return stdout

        if stderr:
            print(f"Error occurred during execution:\n{stderr}")
    exit(1)


class Bot:
    def __init__(self, user, lang, code):
        self.user = int(user)
        self.lang = lang.strip()
        self.code = code
        self.validate()

    def validate(self):
        if self.lang not in LANGS:
            raise BotConfigError(f'Invalid language {self.lang}')
        if int(CONFIG.get('code.size.max', str(len(self.code)+1))) < len(self.code):
            raise BotConfigError(f'Invalid code length {self.lang} (max: {CONFIG["code.size.max"]})')

    def run(self, inp):
        if 'false' != CONFIG.get('offline', 'false'):
            return offline_python(self.code, inp)
        else:
            tio = Tio()
            req = TioRequest(lang=self.lang, code=self.code)
            req.set_input(inp)
            res = tio.send(req)
            if res.error:
                raise BotRunError(res.error)
            else:
                return res.result

    @classmethod
    def load_bots(cls, question_id):
        with open(f'{question_id}_bots.json', 'r') as f:
            errs = []
            for user, bot_config in json.load(f).items():
                try:
                    yield cls(user, **bot_config)
                except BotConfigError as e:
                    err = bot_config.copy()
                    err['error'] = str(e)
                    err['user'] = user
                    errs.append(err)
            if errs:
                with open(f'{question_id}_err.json', 'w') as ef:
                    json.dump(errs, ef, indent=2)

    def __hash__(self):
        return hash(self.user)

    def __eq__(self, other):
        return self.user == other.user

    def __int__(self):
        return int(self.user)

    def __str__(self):
        return f'[{self.user}]'

    def __repr__(self):
        return f'{type(self).__name__}({int(self)})'
