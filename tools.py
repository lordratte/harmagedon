import json
import yaml
import os

import requests as r

CONFIG_FILE = 'config.yml'
CONFIG_FILE_TEMPLATE = f'{CONFIG_FILE}.template'
LANGUAGE_CACHE_FILE = '.langs.json'
CONFIG_ENV_PREFIX = 'harmagedon_'

def get_langs():
    try:
        with open(LANGUAGE_CACHE_FILE, 'r') as f:
            langs = json.load(f)
    except:
        langs = r.get('https://tio.run/languages.json').json()
        with open(LANGUAGE_CACHE_FILE, 'w') as f:
            json.dump(langs, f, indent=2)
    return langs

def get_config():
    try:
        with open(CONFIG_FILE, 'r') as f:
            config = yaml.safe_load(f)
    except SyntaxError:
        print(f'Please copy {CONFIG_FILE_TEMPLATE} to {CONFIG_FILE} and configure your settings before use.')
        exit(1)
    return {k:str(v).lower() for k,v in config.items()}

def get_environ():
    config = {}
    for var,val in os.environ.items():
        var = var.lower()
        if var.startswith(CONFIG_ENV_PREFIX):
            name = var[len(CONFIG_ENV_PREFIX):]
            config[name] = val.lower()

    return config



LANGS = get_langs()
CONFIG = get_config()
CONFIG.update(get_environ())

from pprint import pprint

pprint(CONFIG)
