import itertools
from bot import Bot

STATUSES = {
    'pending': 'Waiting for game to start',
    'started': 'Executing matches',
    'stopped': 'Execution complete',
    'failed': 'Execution did not complete'
}


class GameBase:
    def __init__(self, bots):
        self.bots = list(set(bots))
        self.status = 'pending'
        self.world_state = None

    def run_competition(self) -> list[set[Bot]]:
        ''' Makes the robots compete and returns a list of bot
            victory categories in descending order. e.g. index 0 contains
            a set of first-place bots, index 1 contains second-place bots,
            index n contains the set of last-place bots. If the game does not allow
            for ties, then each set will have a length of 1.
        '''
        self.status = 'started'
        bots = self.bots.copy()
        placements = []
        self.world_state = self.setup_state(bots)

        for i in itertools.count():
            bots_before = set(bots)
            print(f'Tick {i}')
            print(self.world_state)
            self.tick(bots)
            eliminated = bots_before - set(bots)
            if eliminated:
                placements.append(eliminated)
            if len(bots) <= 1:
                placements.append(set(bots))
                break
        print('End state')
        print(self.world_state)

        self.status = 'stopped'
        return placements[::-1]

    def setup_state(self, bots):
        ''' Return the initial state of the world.
        '''
        raise NotImplementedError()

    def tick(self, bots):
        ''' Not expexted to return anything.
            Remove any bots that have been eliminated this tick from
            the `bots` argument.
        '''
        raise NotImplementedError()
