#!/usr/bin/env python
import sys
import json
import requests as r

from tools import CONFIG

from bs4 import BeautifulSoup

def parse_lang(body):
    try:
        return body.find_all('code')[0].text
    except:
        return ''

def parse_code(body):
    try:
        return body.find_all('code')[1].text
    except:
        return ''

def fetch(question_id):
    more = True
    i = 0
    items = []
    site = CONFIG.get('site', 'codegolf')
    while more:
        i += 1
        res = r.get(f'https://api.stackexchange.com/2.3/questions/{question_id}/answers?page={i}&order=desc&sort=votes&site={site}&filter=!SV_d8siTRBMeKDdB9P').json()
        more = res['has_more']
        items += res['items']
    contestants = {}
    print(len(items))
    for ans in items:
        if 'user_id' in ans['owner']:
            contestants[ans['owner']['user_id']] = contestants.get(ans['owner']['user_id'], {})
            body = BeautifulSoup(ans['body'], 'html.parser')
            contestants[ans['owner']['user_id']]['lang'] = parse_lang(body)
            contestants[ans['owner']['user_id']]['code'] = parse_code(body)
    return contestants

if __name__ == '__main__':
    question_id = int(sys.argv[1])
    contestants = fetch(question_id)
    with open(f'{question_id}_bots.json', 'w') as f:
        json.dump(contestants, f, indent=2)

