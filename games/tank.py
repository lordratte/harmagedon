from random import sample

from game import GameBase


class World:

    def __init__(self, bots):
        max_length = len(bots)

        space = zip(sample(range(max_length), max_length),
                    sample(range(max_length), max_length), bots)
        self.world = {b: (x, y) for x, y, b in space}

        self.max_length = max_length

    def wrap_coord(self, v):
        while v < 0:
            v = self.max_length - v
        return v % self.max_length

    def shoot(self, cpos, dirx=0, diry=0):
        (tx, ty) = cpos
        for inc in range(1, self.max_length):
            pos = (tx + inc * dirx, ty + inc * diry)
            for bot, opos in self.world.items():
                if pos == opos:
                    del self.world[bot]
                    return

    def serialize(self):
        return str(self.max_length) + ';'.join([
            ','.join(str(v) for v in (int(b), x, y))
            for b, (x, y) in self.world.items()
        ])

    def act(self, actions):
        # Handle move actions
        for bot, command in actions.items():
            x, y = self.world[bot]
            if command.startswith('m'):
                if command == 'mu':
                    y += 1
                elif command == 'md':
                    y -= 1
                elif command == 'ml':
                    x -= 1
                elif command == 'mr':
                    x += 1

                pos = (self.wrap_coord(x), self.wrap_coord(y))
                if pos not in self.world.values():
                    self.world[bot] = pos

        # Handle shoot actions
        for bot, command in actions.items():
            if bot not in self.world:
                continue
            x, y = self.world[bot]
            if command.startswith('s'):
                pos = x, y
                if command == 'su':
                    self.shoot(pos, diry=1)
                elif command == 'sd':
                    self.shoot(pos, diry=-1)
                elif command == 'sl':
                    self.shoot(pos, dirx=-1)
                elif command == 'sr':
                    self.shoot(pos, dirx=1)

    def __str__(self):
        out = ''
        rev_world = {v: k for k, v in self.world.items()}
        width = 8
        for y in range(self.max_length)[::-1]:
            row = '|'.join([
                str(rev_world.get((x, y), '')).center(width)
                for x in range(self.max_length)
            ])
            out += ('+' + '-' * width) * self.max_length + '+\n'
            out += ('|' + ' ' * width) * self.max_length + '|\n'
            out += f'|{row}|\n'
            out += ('|' + ' ' * width) * self.max_length + '|\n'
        out += ('+' + '-' * width) * self.max_length + '+'
        return out


class Game(GameBase):
    world_state: World

    def setup_state(self, bots):
        return World(bots)

    def tick(self, bots):
        inp = self.world_state.serialize()

        actions = {}
        for bot in bots:
            bot_inp = f'{int(bot)};{inp}'
            actions[bot] = bot.run(bot_inp).strip()
        self.world_state.act(actions)
        bots[:] = [b for b in bots if b in self.world_state.world]
