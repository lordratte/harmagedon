# Tank Battle

Write an AI to control a tank on a grid. Each player's tank is placed in a randomly, unique row and column. The number of the grid's rows and columns are each equal to the number of players.

The grid is a torus. In other words, things exiting the grid from the right-most column will appear on the left-most column (in the same row), things exiting from the upper-most row will appear on the bottom most row (in the same column), and visa-versa.

Tanks receive instructions based on the output of their code. Output is in the form of two letters and a new line.

All tanks act simultaneously in a round of decision-making called a tick. 

## Running code

Every tick, when the simulator needs a tank to make a decision, it runs the tank's code (passing the game state into the standard input). It then captures the standard output and performs that action (as long as it matches a valid instruction for moving or shooting).

There is a 3 second timeout for running code. Any code that takes longer to run will result in that tank not performing any action that tick.

Note: During a tick, all of the move actions will first resolve and then all of the shooting actions.

### Format of Standard Input

The format of the game state will be as follows:

```
World_State ::= YourUserID ';' GridWidth (';' Position)+

Position ::= TankUserID ',' XCoord ',' YCoord
```

As an example, if you are user 9999 on Code Golf Stack Exchange, and the following is the starting grid:

```
    Y
    ^
    |
    +--------+--------+--------+
    |        |        |        |
 2  |  [0]   |        |        |
    |        |        |        |
    +--------+--------+--------+
    |        |        |        |
 1  |        |        | [9999] |
    |        |        |        |
    +--------+--------+--------+
    |        |        |        |
 0  |        |[50000] |        |
    |        |        |        |
    +--------+--------+--------+-> X
        0        1        2
```

Then the input to your code will look like this for the 1st tick:

`9999;3;0,0,2;9999,2,1;50000,1,0`

Breaking it up one chunk at a time:

* `9999`: You are tank 9999.
* `3`: The grid is 3 squares wide and 3 squares tall.
* `0,0,2`: The tank controlled by user 0 is at co-ordinate (0, 2)
* `9999,2,1`: The tank controlled by user 9999 is at co-ordinate (2, 1)
* `50000,1,0`: The tank controlled by user 50000 is at co-ordinate (1, 0)

### Format of code output

#### Moving

* `mu`: Move one tile up
* `md`: Move one tile down
* `ml`: Move one tile left
* `mr`: Move one tile right

If more than one tank attempts to move to the same square in a tick, only one will move there and the others will remain where they were. No grantees are made about which ones stay behind (i.e. in theory non-deterministic but potentially deterministic in implementation).

#### Shooting

* `su`: Shoot upward
* `sd`: Shoot downward
* `sl`: Shoot left
* `sr`: Shoot right

When a tank shoots in a direction, the projectile will instantly traverse from the tank in that direction until it reaches another tank or back to its starting position. If it reaches another tank (other than the one who fired it), it will destroy that tank and remove it from the grid. If no other tanks are encountered along its path, nothing happens.

## Winning

Players are ordered by the amount of time that they last on the grid. The winner is the player who doesn't die, the second placed players are those whose tanks were destroyed last, the third place are those whose tanks were destroyed second-last and so on.

Ties for places happen when more than one tank is destroyed in a tick.

## Submitting

Code should be submitted as an answer on [Code Golf](https://codegolf.stackexchange.com/).

The answer needs to have a very specific format so that the simulator can parse it.

The format can be described as follows.

The answer must contain at least two code blocks. The first code block should be the name of the language being used and the second should be the code being submitted.

The language can be any listed in [this JSON object](https://tio.run/languages.json). Use one of the object keys to refer to which language you will use.

Note: There is a hard 1024 character limit for submissions.

### Example

```````
# This is an example submission

All of this text before the next code block will be ignored:

```
python3
```

The above code block will be parsed to find out which language to run the code in (because it's the first code block); this text, however, will be ignored because it is not a code block.

The next code block will be parsed to get the code that will control this answer's tank. Normally one could add extra information here to elaborate on the solution.

```
import random  
stdin = input()  # Get the world state
                               
# Parse the world state
my_id, width, *tanks = stdin.split(';') 
tanks = [t.split(',') for t in tanks]          
                                               
# Get my position
my_x, my_y = next(t for t in tanks if t[0] == my_id)[1:]  
        
# Get a list of other tanks that are in-line with me
inline = [t for t in tanks if t[0] != my_id and (t[1] == my_x or t[2] == my_y)]  

if inline:      
    # If one exists,
    other_tank = inline[0]
    # then shoot on that axis.
    if other_tank[1] == my_x:
        print('sl')
    else:       
        print('su')
else:           
    # otherwise, move in a random direction
    print(random.choice(['mu', 'md', 'ml', 'mr']))
```

Everything after this previous code block will be ignored (including other `code blocks`).


```````
