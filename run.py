#!/usr/bin/env python
import games
import sys
from bot import Bot

if __name__ == '__main__':
    try:
        bots = Bot.load_bots(sys.argv[2])
        series = getattr(games, sys.argv[1]).Game(bots)
    except IndexError:
        games = [g for g in dir(games) if not g.startswith('_')]
        print('Usage: {} GAME QUESTION_ID\n\
                Where GAME is one of {}\n\
                and QUESTION_ID is the integer ID of the codegolf question\n\
                which contains the competing bots.'.format(sys.argv[0], games))
        exit(1)
    print(series.run_competition())

